# README #

### Design and Implementation Details ###
1. You will need Java 1.8, and Gradle installed to run this
2. You can use whichever database you wish by creating an application-(env).properties file with the collection details - assuming you have that db installed
3. By default this app uses the H2 in-memory database to persist all words and their anagrams.

### Building ###
Building the Anagram project
1.  gradle clean test -Dspring.profiles.active=local

### Running ###
There are multiple ways to run the application:
1. run as java executable, run the command: "java -jar build/libs/anagram-1.0-SNAPSHOT.jar"
2. Local via command line: 'gradle bootRun -Dspring.profiles.active=local'

3.  When you see "me.clark.david.AnagramApplication        : Started AnagramApplication in xxx seconds"  the application is ready for requests

### Running the Anagram project in your IDE: ###
1.  The runnable class is AnagramApplication.java, so you need to direct your IDE to use that as the main() class.
2.  Local via IDE - set the spring.profiles.active=local in your IDE Environment variable
    
### API Documentation ###
The API has been documented using Swagger.  To view the endpoint API, visit http://localhost:3000/swagger-ui.html in a browser

### What is this repository for? ###
* Anagram interview test for Ibotta, assigned to me by Rick Wagner
* 1.0

### How do I get set up? ###
* Summary of set up
** This project is built upon the latest Spring-Boot utilizing the following technologies:
    1. Java
    2. Spring MVC
    3. Spring Data JPA
    4. Hibernate
    5. H2 in-memory database
    6. Goovy for testing with the Spock testing framework
    7. Jackson for handling JSON requests & responses
    8. Swagger API documentation generation
    
* Configuration

* Database configuration
* How to run tests

### Contribution guidelines ###

* Writing tests
    ** all tests are in Spock, which is the Groovy testing framework
    
    
### Suggested Features ###
1. Docker - I did the initial work to Dockerize this application.  Some further tweaking is needed (and I hope to finish it soon)
2. Persistent Data Store - Currently H2 is used as an in-memory data store, but idealy something like Mongo or Postgres 
    could be used for different environments
3. Environment specific settings - Having different configurations for different environments is easily accomplished in Spring
    and would allow features such as different databases per environment
4. True anagram testing using the entire dictionary.  As noted in the code, some words return more anagrams than the tests allow


### Design overview and trade-offs ###
I made two decisions that affected my design and effort:
1.  Anagram Generation.  My first attempt to generate anagrams was based on the assumptions of permutations.  However, this
    quickly ran into performance issues.  For example, "anamorphosis" has 12 letters, which gives 12! = 479,001,600 permutations. 
    Each string takes at least 12 bytes (assuming, say, UTF-8 with ASCII characters only), meaning a total size of 12 * 479,001,600 bytes, which is roughly 6 GB.
    My testing would literally run out of memory by the third word, which was "anamorphosis".
    My solution was to use a dictonary-word algorithm that looked up each word in a data structure.
2.  I chose to use a database rather than a simple in-memory data structure.  This allowed me to show database interaction
    via JPA. 
     
### Edge Cases ###
I found two scenarios that caused me some issues:
1. The aforementioned anagram generation.  My calculations generated more anagrams than the test allowed
2. The JSON requests to the API were in a format I was not familiar with using Spring or Jersey endpoints.  I ended up
    creating a Words class that would receive the requests properly, and manually parsing the strings into JSON format
    that the tests would accept.  Normally I would just return a ResponseEntity with a List of Strings and Jackson JSON
    formatter outputs them in a format usable by jQuery and other Javascript parsers.
3.  I did find that the default Ruby version installed on my OS X would not execute the provided tests.  I upgraded it
    and they worked properly afterwards.
    
###  Limits on the length of words ###
I did not find any limits on the length of words once I changed my anagram calculations to use a map.
    
### Who do I talk to? ###

* David Clark (david@clanclark.net)
