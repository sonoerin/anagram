package me.clark.david.utility;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import javax.annotation.PostConstruct;
import me.clark.david.service.AnagramService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

/**
 * File:
 * Description: Startup class that will read the contents of the file, and parse it for insertion into the database.
 * Author:      David Clark
 */
@Component
public class DictionarySeed {

    private static final Logger LOGGER = LoggerFactory.getLogger(DictionarySeed.class);
    String fileName = "dictionary.txt";

    @Autowired
    private AnagramService anagramService;

    /**
     * Attempt to read the file on startup, and if it fails log and error and throw the exception causing the app to stop
     */
    @PostConstruct
    public void init() throws Exception {

        Set<String> uniqueWords = parseDictionaryFile();
         anagramService.createAnagrams(uniqueWords);

    }

    public Set<String> parseDictionaryFile() throws Exception {
        Set<String> uniqueWords = new HashSet<>();
        try {
            ClassPathResource resource = new ClassPathResource(fileName);
            Scanner scanner = new Scanner(resource.getInputStream());
            while (scanner.hasNext()) {
                String s = scanner.nextLine();
                uniqueWords.add(s.toLowerCase());

            }
          return uniqueWords;
        } catch (Exception ex) {
            LOGGER.error("Error parsing {} file: {}", fileName, ex.getLocalizedMessage());
            throw ex;
        }
    }
}
