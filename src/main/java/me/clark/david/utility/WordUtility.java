package me.clark.david.utility;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import me.clark.david.database.Word;

/**
 * File:        WordUtility
 * Description: Utility class for parsing Word objects into desired results
 * Author:      David Clark on 2/28/17.
 */
public final class WordUtility {

    public static String sortString(String goodString) {

        char[] letters = goodString.toLowerCase().toCharArray();
        Arrays.sort(letters);
        return new String(letters);
    }

    /**
     * Take a list of Strings and convert them to Word entities that are suitable for database use
     * @param strings
     * @return
     */
    public static List<Word> convertStringsToWords(List<String> strings) {

        final List<Word> words = strings.stream().map(string -> new Word(string)).collect(Collectors.toList());
        return words;
    }

    /**
     * Take a list of Words and parse them into json appropriate String.  This means taking the value of the Word,
     * and wrapping it in double quotes.
     *
     * @param words List of Word entities
     * @return A String with the value of every Word wrapped in double quotes
     */
    public static  String parseIntoResults(List<Word> words) {
        String result = words.stream().map((s) -> "\"" + s.getValue() + "\"").collect(Collectors.joining(", "));

        return result;
    }

    /**
     * Here we only have a single word, so we want to use all the anagram values rather than the word, which is just the key
     * @param word A Word with a value of the hashkey, and values are the anagrams
     * @return A String with the value of every anagram wrapped in double quotes
     */
    public static String parseSingleWordIntoResults(Word word) {
        return word.getAnagrams().stream().map((s) -> s ).collect(Collectors.joining("\", \""));
    }

}
