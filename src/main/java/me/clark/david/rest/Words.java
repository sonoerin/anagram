package me.clark.david.rest;

import java.util.List;

/**
 * File:
 * Description: Class that holds json requests.  This is necessary because of the format (*.json) they are submitted
 *              which demands this.
 * Author:          David Clark
 */
public class Words {

    private List<String> words;

    public List<String> getWords() {

        return words;
    }

    public void setWords(List<String> words) {

        this.words = words;
    }

}
