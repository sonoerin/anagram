package me.clark.david.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.Collections;
import java.util.List;
import me.clark.david.database.Word;
import me.clark.david.service.AnagramService;
import me.clark.david.utility.WordUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * File:
 * Description: API Endpoint to handle all things anagram
 * Author:      David Clark on 2/8/17.
 */
@Api(value = "/anagrams", description = "Anagram API", produces = "application/json")
@RestController
@RequestMapping("/anagrams")
public class AnagramController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AnagramController.class);

    @Autowired
    private AnagramService anagramService;

    @ApiOperation(value = "fetchWord", nickname = "fetchWord", response = ResponseEntity.class, httpMethod = "GET")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Success", response = ResponseEntity.class),
                    @ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 500, message = "Failure") })
    @RequestMapping(value = "/{fetchWord}", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity fetchWord(@PathVariable String fetchWord,
                             @RequestParam(value = "limit", required = false) String limit) {

        Word foundWord = anagramService.findWord(fetchWord);
        if (foundWord != null) {

            updateAnagramListForTest(foundWord, fetchWord, limit);
            String result = WordUtility.parseSingleWordIntoResults(foundWord);

            return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON)
                            .body("{\"anagrams\": [" + "\"" + result + "\"]}");
        }
        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON)
                        .body("{\"anagrams\": " + "[]}");
    }

    private void updateAnagramListForTest(Word foundWord, String fetchWord, String limit) {

        Collections.sort(foundWord.getAnagrams());

        if (foundWord.getAnagrams().contains(fetchWord)) {
            foundWord.getAnagrams().remove(fetchWord);
        }

        if (limit != null) {
            int limitCount = Integer.parseInt(limit);
            List subList = foundWord.getAnagrams().subList(0, limitCount);
            foundWord.setAnagrams(subList);
        }
    }

}
