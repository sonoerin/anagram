package me.clark.david.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import me.clark.david.database.Word;
import me.clark.david.database.WordsRepository;
import me.clark.david.service.AnagramService;
import me.clark.david.utility.WordUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * File:        WordsController
 * Description: API endpoint for all things word related.
 * Author:      David Clark
 */
@Api(value = "/words", description = "Words API", produces = "application/json")
@RestController
@RequestMapping("/words")
public class WordsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(WordsController.class);

    @Autowired
    private WordsRepository wordsRepository;

    @Autowired
    private AnagramService anagramService;

    @ApiOperation(value = "deleteWord", nickname = "deleteWord", response = ResponseEntity.class, httpMethod = "DELETE")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Success", response = ResponseEntity.class),
                    @ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 500, message = "Failure") })
    @RequestMapping(value = "/{deleteWord}", method = RequestMethod.DELETE)
    public @ResponseBody ResponseEntity deleteWord(@PathVariable String deleteWord) {

        anagramService.deleteWord(deleteWord);
        LOGGER.info("Deleted {}", deleteWord);
        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON)
                        .body("{\"deleted word\": " + "\"" + deleteWord + "\"}");
    }

    @ApiOperation(value = "addWordsToCorpus", nickname = "addWordsToCorpus", response = ResponseEntity.class,
                  httpMethod = "POST")
    @ApiResponses(value = { @ApiResponse(code = 201, message = "Success", response = ResponseEntity.class),
                    @ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 500, message = "Failure") })
    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody ResponseEntity addWordsToCorpus(@RequestBody Words words) {

        List<Word> wordList = WordUtility.convertStringsToWords(words.getWords());
        anagramService.updateWords(wordList);
        LOGGER.info("Successfully added {} words to corpus repository", wordList.size());
        return ResponseEntity.status(HttpStatus.CREATED).contentType(MediaType.APPLICATION_JSON)
                        .body("{\"added words\": [" + WordUtility.parseIntoResults(wordList) + "]}");
    }

    @ApiOperation(value = "getAllWords", nickname = "getAllWords", response = ResponseEntity.class, httpMethod = "GET")
    @ApiResponses(value = { @ApiResponse(code = 200, message = "Success", response = ResponseEntity.class),
                    @ApiResponse(code = 500, message = "Failure") })
    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody public ResponseEntity getAllWords() {

        List<Word> join = wordsRepository.findAll();
        LOGGER.info("Successfully found {} words ", join.size());
        return ResponseEntity.status(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON)
                        .body("{\"Success\": [" + WordUtility.parseIntoResults(join) + "]}");
    }

    @ApiOperation(value = "deleteAllWords", nickname = "deleteAllWords", response = ResponseEntity.class,
                  httpMethod = "GET")
    @ApiResponses(value = { @ApiResponse(code = 204, message = "Success", response = ResponseEntity.class),
                    @ApiResponse(code = 500, message = "Failure") })
    @RequestMapping(method = RequestMethod.DELETE)
    @ResponseBody public ResponseEntity deleteAllWords() {

        try {
            anagramService.deleteAllWords();
            LOGGER.info("Successfully truncated word repository");
            return ResponseEntity.status(HttpStatus.NO_CONTENT).contentType(MediaType.APPLICATION_JSON)
                            .body("{\"Success\": \"Deleted all words\"}");
        } catch (Exception ex) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).contentType(MediaType.APPLICATION_JSON)
                            .body("{\"Failure\":[" + ex.getLocalizedMessage() + "] }");
        }
    }
}
