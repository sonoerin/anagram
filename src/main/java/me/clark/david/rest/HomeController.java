package me.clark.david.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import me.clark.david.database.WordsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * File:        HomeController
 * Description: API endpoint for a single endpoint that deletes all the words.
 * Author:      David Clark
 */
@Api(value = "/", description = "Delete Words API", produces = "application/json")
@RestController
public class HomeController {

    private static final Logger LOGGER = LoggerFactory.getLogger(HomeController.class);

    @Autowired
    private WordsRepository wordsRepository;

    @ApiOperation(value = "deleteAllWords", nickname = "deleteAllWords", response = ResponseEntity.class,
                  httpMethod = "DELETE")
    @ApiResponses(value = { @ApiResponse(code = 204, message = "Success", response = ResponseEntity.class),
                    @ApiResponse(code = 400, message = "Bad Request"), @ApiResponse(code = 500, message = "Failure") })
    @RequestMapping(value = "/", method = RequestMethod.DELETE)
    public @ResponseBody ResponseEntity deleteAllWords() {

        wordsRepository.deleteAll();
        LOGGER.info("Deleted all words from Corpus");
        return ResponseEntity.status(HttpStatus.NO_CONTENT).contentType(MediaType.APPLICATION_JSON)
                        .body("{\"deleted words\":  \"success+ \"}");
    }
}
