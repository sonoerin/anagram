package me.clark.david.config;

import com.mongodb.Mongo;
import com.mongodb.MongoClientOptions;
import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodProcess;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.IMongodConfig;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.distribution.Version;
import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

/**
 * File:        MongoConfig
 * Description: Configure an embedded MongoDB to be used for tests and "production" runs.  I chose Mongo as
 *              the persistence store because the data model (strings) didn't work with an RMDBS like MySQL
 *              or H2. This class tells the container how to startup & shutdown Mongo.  There appears to be a
 *              known issue with the driver shutting down while in use with Spring Boot. As this only affect shutdown
 *              and not normal use, I decided it was worth the stack trace.
 * Author:      David Clark
 */
@Configuration
public class MongoConfig {

    private static final MongodStarter starter = MongodStarter.getDefaultInstance();

    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    private MongoProperties properties;

    @Autowired(required = false)
    private MongoClientOptions options;

    @Autowired
    private Environment environment;

    @Bean(destroyMethod = "close")
    public Mongo mongo() throws IOException {

        Net net = mongod().getConfig().net();
        properties.setHost(net.getServerAddress().getHostName());
        properties.setPort(net.getPort());
        return properties.createMongoClient(this.options, environment);
    }

    @Bean(destroyMethod = "stop")
    public MongodProcess mongod() throws IOException {

        return mongodExe().start();
    }

    @Bean(destroyMethod = "stop")
    public MongodExecutable mongodExe() throws IOException {

        return starter.prepare(mongodConfig());
    }

    @Bean
    public IMongodConfig mongodConfig() throws IOException {

        return new MongodConfigBuilder().version(Version.Main.PRODUCTION).build();
    }

}
