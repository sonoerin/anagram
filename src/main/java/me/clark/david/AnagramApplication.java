package me.clark.david;

import me.clark.david.rest.WordsController;
import me.clark.david.service.AnagramService;
import me.clark.david.utility.DictionarySeed;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * File:        AnagramApplication
 * Description: Main class that runs this application and configures Swagger (API documentation)
 * Author:      David Clark
 */
@SpringBootApplication
@EnableSwagger2
@ComponentScan(basePackageClasses = { WordsController.class, DictionarySeed.class, AnagramService.class })
public class AnagramApplication {


    public static void main(String[] args) {

        SpringApplication.run(AnagramApplication.class, args);
    }

    /**
     * Use of Swagger allows API documentation to be made available by hitting the endpoint: http://localhost:3000/swagger-ui.html
     * When the Spring container picks up the Swagger Docket Bean, the "any" make API available to Swagger
     */
    @Bean
    public Docket api() {

        return new Docket(DocumentationType.SWAGGER_2).apiInfo(apiInfo()).select().apis(RequestHandlerSelectors.any())
                        .paths(PathSelectors.any()).build();
    }

    private ApiInfo apiInfo() {

        return new ApiInfoBuilder().title("Anagram API Documentation ")
                        .description("Swagger API Documentation provided for your viewing pleasure").version("1.0")
                        .build();
    }

}
