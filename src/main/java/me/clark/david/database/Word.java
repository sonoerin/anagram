package me.clark.david.database;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * File:        Word
 * Description: Entity for a String value encapsulated as a Word.  By wrapping the value in an
 *              entity, we get an id for quick searches, and associating known anagrams
 * Author:      David Clark
 */
@Document(collection = "words")
public class Word {

    @Id
    private String id;
    private String value;
    private List<String> anagrams = new ArrayList<>();

    public Word() {

    }

    public Word(String value) {

        this.value = value;
    }

    public Word(String value, List anagrams) {

        this.value = value;
        this.anagrams.addAll(anagrams);
    }

    public String getValue() {

        return value;
    }

    public void setValue(String value) {

        this.value = value;
    }

    public List<String> getAnagrams() {

        return anagrams;
    }

    public void setAnagrams(List<String> anagrams) {

        this.anagrams = anagrams;
    }
}
