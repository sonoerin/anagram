package me.clark.david.database;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;

/**
 * File:            WordsRepository
 * Description:     Spring Data JPA for MongoDB.  Use this interface to do all interaction with MongoDB
 * Author:          David Clark
 */
public interface WordsRepository extends MongoRepository<Word, String> {

    Word findByValue(@Param("word") String word);
}
