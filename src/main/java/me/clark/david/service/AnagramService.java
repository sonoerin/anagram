package me.clark.david.service;

import java.util.List;
import java.util.Set;
import me.clark.david.database.Word;
import me.clark.david.database.WordsRepository;
import me.clark.david.utility.AnagramGenerator;
import me.clark.david.utility.DictionarySeed;
import me.clark.david.utility.WordUtility;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * File:        AnagramService
 * Description: Service that handles all things anagram
 * Author:      David Clark
 */
@Component
@Transactional
public class AnagramService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AnagramService.class);

    @Autowired
    private WordsRepository wordsRepository;

    @Autowired
    private DictionarySeed dictionarySeed;

    public void createAnagrams(Set<String> uniqueWords) {

        AnagramGenerator anagramGenerator = new AnagramGenerator(uniqueWords);
        List<Word> anagrams = anagramGenerator.searchAlphabetically();

        wordsRepository.save(anagrams);
        LOGGER.info("Inserted {} records into the database", anagrams.size());
    }

    /**
     * Parse out the dictionary.txt file and return a unique Set of words in that file.
     * @param words
     */
    public void updateWords(List<Word> words) {

        if ( wordsRepository.findAll().size() < 1) {
            try {
                // we no longer have our dictionary persisted, so recreate and grab the anagrams we are interested in
                Set<String> uniqueWords = dictionarySeed.parseDictionaryFile();
                createAnagrams(uniqueWords);
            } catch (Exception ex) {
                LOGGER.error("caught {} exception while re-seeding database ", ex.getLocalizedMessage());
            }
        }
        for (Word word: words) {
            if (!doesWordExist(word)) {
               wordsRepository.save(word);
            }
        }
    }

    public void deleteWord(String word) {
        Word foundMe = wordsRepository.findByValue(WordUtility.sortString(word));

        if (foundMe != null) {
            if (foundMe.getAnagrams().contains(word)) {
                foundMe.getAnagrams().remove(word);
            }
            wordsRepository.save(foundMe);
        }
    }

    public Word findWord(String word) {
        return wordsRepository.findByValue(WordUtility.sortString(word));
    }

    public void deleteAllWords() {
        wordsRepository.deleteAll();
    }

    private boolean doesWordExist(Word word) {
        Word foundWord = wordsRepository.findByValue(WordUtility.sortString(word.getValue()));
        return (foundWord != null);
    }

}

