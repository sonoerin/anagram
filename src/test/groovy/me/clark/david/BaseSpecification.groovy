package me.clark.david

import me.clark.david.AnagramApplication
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import spock.lang.Specification

/**
 * File:
 * Description:  Base Class for all Integration Tests
 * Author:       David Clark on 2/8/17.
 */

@ContextConfiguration(classes = AnagramApplication.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@ActiveProfiles("test")
class BaseSpecification extends Specification {

}
