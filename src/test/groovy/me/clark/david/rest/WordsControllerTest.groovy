package me.clark.david.rest

import groovyx.net.http.ContentType
import groovyx.net.http.RESTClient
import me.clark.david.BaseSpecification

/**
 * File:         WordsControllerTest
 * Description:  Integration Test for the /words endpoint
 * Author:       David Clark
 */
class WordsControllerTest extends BaseSpecification {

    def primerEndpoint = new RESTClient("http://localhost:3000/")

    def "test the GET endpoint is available"() {

        when:
        def resp = primerEndpoint.get([path: '/words'])

        then:
        with(resp) {
            status == 200
            contentType == "application/json"
        }
        with(resp.data) {
            payload == null
        }
    }

    def "try posting a list and check the response code & contentType "() {

        when:
        def resp = primerEndpoint.post([path: '/words', body: newWords, requestContentType: ContentType.JSON])

        then:
        with(resp) {
            status == statusCode
            contentType == "application/json"
        }

        where:
        statusCode | newWords
        201        | new Words(words: [""])
        201        | new Words(words: ["David"])
        201        | new Words(words: ["David", "Clark"])
    }

    def "test deleting a single word from the endpoint"() {

        def client = new RESTClient('http://localhost:3000/words/' + newWords)

        when:
        def resp = client.delete(requestContentType: ContentType.JSON)

        then:
        with(resp) {
            status == statusCode
            contentType == "application/json"
        }

        where:
        statusCode | newWords
        200        | "David"
    }


    def "test deleting all words from the endpoint"() {
        def client = new RESTClient('http://localhost:3000/words')

        when:
        def resp = client.delete(requestContentType: ContentType.JSON)


        then:
        with(resp) {
            status == 204
        }

        and:
        when:
        def resp1 = primerEndpoint.get([path: '/words'])

        then:
        with(resp1) {
            status == 200
            contentType == "application/json"
        }
        with(resp1.data) {
            payload == null
        }

    }
}
