package me.clark.david.rest

import groovyx.net.http.ContentType
import groovyx.net.http.RESTClient
import me.clark.david.BaseSpecification

/**
 * File:         HomeControllerTest
 * Description:  Integration Test for the / endpoint
 * Author:       David Clark
 */
class HomeControllerTest extends BaseSpecification {

    def "test deleting all the words from the endpoint"() {

        def client = new RESTClient('http://localhost:3000/')

        when:
        def resp = client.delete(requestContentType: ContentType.JSON)

        then:
        with(resp) {
            status == 204
        }
    }
}
