package me.clark.david.rest

import me.clark.david.BaseSpecification
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.context.WebApplicationContext

/**
 * File:
 * Description: Small test to confirm that the context is booted properly and all integration tests will run
 * Author:      David Clark on 9/22/16.
 */
class ApplicationSpecWithoutAnnotation extends BaseSpecification {

    @Autowired
    WebApplicationContext context

    def "test boot up is succerssful without errors"() {

        expect: "web application context exists"
        context != null
    }
}
