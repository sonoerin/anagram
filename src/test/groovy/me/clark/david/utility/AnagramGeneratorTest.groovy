package me.clark.david.utility

import spock.lang.Specification

/**
 * File:
 * Description: 
 * Author: David Clark on 2/27/17.
 */
class AnagramGeneratorTest extends Specification {

    def "test that a single letter word results in a word without anagram"() {

        Set dictionary =   ["a"]

        def anagramGenerator = new AnagramGenerator(dictionary)
        when:
        def result = anagramGenerator.searchAlphabetically()

        then:
        result.size() == 1
        result[0].value == "a"
        result[0].anagrams == []
    }

    def "test that a word results in a word with two anagram for apple"() {

        Set dictionary =   ["a", "apple", "appel" ,"pepla"]
        def anagramGenerator = new AnagramGenerator(dictionary)
        when:
        def result = anagramGenerator.searchAlphabetically()
        then:
        result.size() == 2
        result[0].value == "a"
        result[0].anagrams == []

        result[1].value == "aelpp"
        result[1].anagrams.contains("appel")
        result[1].anagrams.contains("pepla")
        result[1].anagrams.size() ==3

    }

}
